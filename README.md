# 15-inch Autonomous

Here are all the programs developed for the 15-inch robot autonomous throughout the season. These programs include simulations, PID tuning, tests and competition programs.

## Code organization

For a better organization these will be divided into 3 main folders corresponding to the 3 tournaments held until 2023 VEX Robotics World Championship.

* Ciudad Madero Regional Tournament
* León Guanajuato International Championship
* Mérida Yucatán National Championship

Within the main folders, subfolders will be created where the simulation, tuning and test programs will be stored.